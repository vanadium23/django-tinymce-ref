Django Tinymce Refactored
=============

What changed:

  * settings now in conf.py and driven by django-autoconf
  * ModelField is deprecated
  * Base level changed from tinymce to django_tinymce

How to test:

  * Clone repo
  * Make virtualenv
  * `pip intall Django django-autoconf`
  * `cd example`
  * `./manage.py migrate`
  * `./manage.py createsuperuser`
  * `./manage.py runserver`
