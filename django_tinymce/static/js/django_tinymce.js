(function ($) {
    $(function () {
        $('.django-tinymce').each(function () {
            $e = $(this);
            var django_tinymce_conf = $.parseJSON($e.attr('data-django-tinymce-config'));
            tinymce.init(django_tinymce_conf);
        });
    });
}((typeof django === 'undefined' || typeof django.jQuery === 'undefined') && jQuery || django && django.jQuery));